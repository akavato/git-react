import React, { useEffect } from 'react';
import { Input, Avatar, Table } from 'antd';

const { Search } = Input;

const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      render: (link) => <a href="{link.href}">{link.name}</a>,
      sorter: {
        compare: (a, b) => a.name.localeCompare(b.name)
      },
    },
    {
      title: 'Stars',
      dataIndex: 'stargazers_count',
      width: '20%',
      render: (stars) => <span>{stars}⭐</span>,
      sorter: {
        compare: (a, b) => a.stars - b.stars
      },
    }
];

const data = [];

const apiAdress = 'https://api.github.com';
let user = {};
let repos = [];


async function searchUser(val) {
    // this.loading = true;
    try {
        const resUser = await fetch(`${apiAdress}/users/${val}`);
        user = await resUser.json();
        const resRepos = await fetch(`${user.repos_url}?per_page=9`);
        repos = await resRepos.json();
        // repos.forEach((elem, i) => {
        //     data.push({ key: i, name: elem.name, href: elem.html_url, stars: elem.stargazers_count })
        // });
    }
    catch (e) {
        throw new Error('smth goes wrong');
    }
    // this.loading = false;
};

function App() {
    useEffect(() => {
    });

    return (
        <div className="container">
            <Search
                className="search"
                placeholder="search user"
                enterButton="Search"
                size="large"
                onSearch={value => searchUser(value)}
                />

            <div className="user-info">
                <Avatar src={user.avatar_url} />
                <div className="text-info">
                    <div className="name">
                        {user.name}
                    </div>
                    <div className="puplic-repos">
                        public repos: {user.public_repos}
                    </div>
                </div>
            </div>

            <Table
                className="data-table"
                columns={ columns }
                dataSource={ repos }
                rowKey={record => record.id}
                pagination={{
                    position: 'bottomCenter',
                    defaultPageSize: 3,
                    simple: true
                }}
            />
        </div>
    );
}

export default App;
